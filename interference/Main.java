
package ru.demo.interference;

/**
 * Запуск примера, демонстрирующего потерю данных
 */
public class Main {
    public static void main(String[] args) throws InterruptedException {
        new InterferenceExample().example();
    }
}