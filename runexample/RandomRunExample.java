package ru.demo.runexample;

import ru.demo.hellothread.Thread;

/**
 * Created by Elena on 03.11.2017.
 */
public class RandomRunExample extends Thread {
    public void run() {
        //получение имени текущего потока
        System.out.println(Thread.currentThread().getName());
    }

    //создание и запуск 10 потоков
    public static void example() {
        for (int i = 0; i < 10; i++) {
            Thread thread = new RandomRunExample();
            thread.start();
        }
    }
}
