package ru.demo.runningmultiplethreads;

/**
 * Created by Elena on 21.11.2017.
 */
public class Main {
    public static void main(String [] args) throws Exception{
        new MThread().start();
        new MThread().start();
        new MThread().start();
        new MThread().start();
    }
}
class MThread extends Thread{
    @Override
    public void run(){
        for(int i=0; i<200;i++){
            System.out.println("Thread name is- "+ Thread.currentThread().getName()+"i= "+i);
        }
    }
}
