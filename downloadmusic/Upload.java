package ru.demo.downloadmusic;
        import java.io.FileOutputStream;
        import java.io.IOException;
        import java.net.URL;
        import java.nio.channels.Channels;
        import java.nio.channels.ReadableByteChannel;
/**
 * Скачивание файлов с помощью потоков
 * Created by Elena on 14.12.2017.
 */
public class Upload extends Thread {
    private String strUrl;
    private String loading;

    /**
     * Конструктор по умолчанию
     * @param strUrl
     * @param loading
     */
    Upload(String strUrl, String loading) {
        this.strUrl = strUrl;
        this.loading = loading;
    }

    /**
     * Скачивается музыка
     */
    public void run() {
        try {
            URL url = new URL(strUrl);
            ReadableByteChannel byteChannel = Channels.newChannel(url.openStream());
            FileOutputStream stream = new FileOutputStream(loading);
            stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
            stream.close();
            byteChannel.close();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}