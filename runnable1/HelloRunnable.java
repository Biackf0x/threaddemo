package ru.demo.runnable1;

import java.lang.*;

/**
 * Реализация интерфейса Runnable
 * Интерфейс Runnable определяет один метод run,
 * Предназначен для
 * Runnable-объект, который
 * и с помощью метода start()поток запускается
 */
public class HelloRunnable extends ru.demo.runnable1.Runnable {
    public void run(){
        System.out.println("Hello from a thread!");
    }
public static void main(String args[]){
    (new Thread((java.lang.Runnable) new HelloRunnable())).start();
    System.out.println("Hello from a thread!");
}
}
