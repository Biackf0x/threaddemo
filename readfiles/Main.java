package ru.demo.readfiles;
import java.io.*;
import java.util.concurrent.Semaphore;

public class Main {
    public static void main(String[] args) throws InterruptedException, IOException {
    try (DataInputStream input1 = new DataInputStream(new FileInputStream("input5.txt"))) {
        try (DataInputStream input2 = new DataInputStream(new FileInputStream("input6.txt"))) {
            try (DataInputStream input4 = new DataInputStream(new FileInputStream("input7.txt"))) {
                try (DataOutputStream output = new DataOutputStream(new FileOutputStream("output8.txt"))) {
                    Semaphore sem = new Semaphore(2);
                    ReadFile thread1 = new ReadFile(input1, output, sem);
                    ReadFile thread2 = new ReadFile(input2, output, sem);
                    ReadFile thread3 = new ReadFile(input4, output, sem);
                    thread1.setPriority(10);
                    thread2.setPriority(5);
                    thread3.setPriority(3);
                    thread1.start();
                    thread2.start();
                    thread3.start();
                    thread1.join();
                    thread2.join();
                    thread3.join();

                }
            }
        }

    }
}
}