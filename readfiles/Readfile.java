package ru.demo.readfiles;

import java.io.*;
import java.util.concurrent.Semaphore;

/**
 * Semaphore-средство реализации для доступа к общему ресурсу
 */
public class ReadFile extends Thread {
    Semaphore sem;
    private DataInputStream input;
    private DataOutputStream output;

    public ReadFile(DataInputStream input, DataOutputStream output, Semaphore sem) {
        this.input = input;
        this.output = output;
        this.sem = sem;
    }

    public void run() {
        String str;
        try {
            while (input.available() != 0) {//проверят доступное кол-во байт для чтения
                try {
                    sem.acquire();//разрешает доступ для доступа к общему ресурсу(ничего не возвращает,тк void)
                    str = input.readLine();
                    output.writeChars(str + "\n");
                    sem.release();//Забирает доступ
                    Thread.currentThread().sleep(10);
                } catch (InterruptedException ex) {
                }
            }

        } catch (IOException ex) {


        }
    }
}