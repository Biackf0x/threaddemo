package ru.demo.eggorchicken;

import java.util.Random;

/**
 * Created by Elena on 02.11.2017.
 */
public class ChickenEgg {
    public static int getTimeSleep() {
        final Random random = new Random();
        int tm = random.nextInt(1000);
        if (tm < 10)
            tm *= 100;
        else if (tm < 100)
            tm *= 10;
        return tm;
    }
    public static void main(String[] args)
    {
        Egg egg = new Egg (); // Создаем поток
        System.out.println("Кто появился первым ?");

        egg.start();
        for(int i = 0; i < 5; i++) {
            try {
                Thread.sleep(ChickenEgg.getTimeSleep());// Приостанавливаем поток
                System.out.println("Курица");
            }catch(InterruptedException e){}
        }
        if(egg.isAlive()) { // Cказало ли яйцо последнее слово?
            try{
                egg.join(); // Ждем, пока яйцо закончит высказываться
            }catch(InterruptedException e){}

            System.out.println("Первым появилось яйцо !!!");
        } else
        {
            System.out.println("Первой появилась курица !!!");
        }
        System.out.println("Спор закончен");
    }
}