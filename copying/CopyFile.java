package ru.demo.copying;

import java.io.*;

/**
 * Created by Elena on 27.11.2017.
 */
public class CopyFile extends Thread {
    private String input;
    private String output;

    public CopyFile(String input, String output) {
        this.input = input;
        this.output = output;
    }


    public void run() {
        String str;
        try {
            BufferedReader input = new BufferedReader(new FileReader(this.input));
            BufferedWriter output = new BufferedWriter(new FileWriter(this.output));

            while ((str=input.readLine()) !=null) {
                output.write(str + "\n");
            }
            output.flush();
        } catch (IOException ignored) {
        }
    }
}