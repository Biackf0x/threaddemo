package ru.demo.copying;

import java.io.IOException;

/**
 * Created by Elena on 28.11.2017.
 */
public class Main {
        public static void main(String[] args) throws InterruptedException, IOException {
            CopyFile thread1 = new CopyFile("input1.txt", "output2.txt");
            CopyFile thread2 = new CopyFile("input1.txt", "output21.txt");
            final long before = System.currentTimeMillis();
            thread1.start();
            thread2.start();
            final long after1 = System.currentTimeMillis();
            System.out.println("Время поока после запуска = " + (after1 - before));
            thread1.join();
            thread2.join();
            final long after = System.currentTimeMillis();
            System.out.println("Время потока по завершению = " + (after - before));
        }
}