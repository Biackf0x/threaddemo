package ru.demo.catchup;

/**
 * Created by Elena on 06.11.2017.
 */
public class PersikGame    //Класс с методом main()
{
    static Tyapa mAnotherOpinion;	//Побочный поток
    public static void main(String[] args) {
        mAnotherOpinion = new Tyapa();	//Создание потока
        System.out.println("Марш!!!");
        mAnotherOpinion.start(); 			//Запуск потока
        for(int i = 0; i < 10; i++){
            try{
                Thread.sleep(1000);		//Приостанавливает поток на 1 секунду
            }catch(InterruptedException e){}
            System.out.println("Персик, ход:"+i);}
        if(mAnotherOpinion.isAlive()){ //пока не выполнится цикл for(i=10)
            try{
                mAnotherOpinion.join();
            }catch(InterruptedException e){}
            System.out.println("Персик победил!");
        }else{
            System.out.println("Тяпа победил! ");
        }
        System.out.println("Забег окончен.");
    }
}