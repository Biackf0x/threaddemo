package ru.demo.hellothread;

/**
 * Создание подкласса Thread
 *
 * Класс Tread реализует интерфейс Runnable, хотя его метод run()ничего не делает.
 * Покласс класса Thread может обеспечить собственную реализацию метода run()
 */
public class Thread extends java.lang.Thread {
    public void run(){
        System.out.println("Hello form a thread!");
    }
    public static void main (String args[]){
        (new Thread()).start();
        System.out.println("Hello form a thread!");
    }

}
